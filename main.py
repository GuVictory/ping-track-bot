import logging
import asyncio

from aiogram.dispatcher import FSMContext
from dotenv import dotenv_values
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.dispatcher.filters.state import State, StatesGroup

from texts import start_text

config = dotenv_values(".env")

# Задаем уровень логов
logging.basicConfig(level=logging.INFO)

# Инициализируем бота
loop = asyncio.get_event_loop()
bot = Bot(token=config.get('BOT_TOKEN'), loop=loop, parse_mode=types.ParseMode.HTML)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
dp.middleware.setup(LoggingMiddleware())


# States
class TomatoTimer(StatesGroup):
    work_time = State()  # Will be represented in storage as 'TomatoTimer:work_time'
    s_rest_time = State()  # Will be represented in storage as 'TomatoTimer:s_rest_time'
    total_tomatos = State()


@dp.message_handler(commands='start')
async def cmd_wtf(message: types.Message):
    await bot.send_message(chat_id=message.chat.id, text=start_text(message.from_user.username))


@dp.message_handler(commands='wtf')
async def cmd_start(message: types.Message):
    await bot.send_message(chat_id=message.chat.id, text="Не, пока не объясню 💩")


@dp.message_handler(commands='start_tomato')
async def cmd_start_tomato(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        await bot.send_message(chat_id=message.chat.id,
                               text=f"⌛ Таймер для работы на {data['work_time']} минут поставлен!")
        await asyncio.sleep(data['work_time'] * 60)  # TODO: добавить * 60

        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Да, включи таймер на отдых")
        markup.add("Нет, погнали следующий рабочий цикл")
        markup.add("Нет, на сегодня хватит!")
        markup.one_time_keyboard = True

        await bot.send_message(chat_id=message.chat.id, text="🥳 Пора отдохнуть, не так ли?",  reply_markup=markup)


@dp.message_handler(commands='config_tomato')
async def cmd_new_tomato(message: types.Message):
    await TomatoTimer.work_time.set()
    await bot.send_message(chat_id=message.chat.id, text="Ок, сколько минут будет у нас время работы?")


# Установка таймера работы
@dp.message_handler(lambda message: not message.text.isdigit(), state=TomatoTimer.work_time)
async def process_work_time_invalid(message: types.Message):
    return await message.reply("Время работы нужно бы указать числом в минутах\n"
                               "Так что, сколько минут работаем?")


@dp.message_handler(lambda message: message.text.isdigit(), state=TomatoTimer.work_time)
async def process_work_time(message: types.Message, state: FSMContext):
    await TomatoTimer.next()
    await state.update_data(work_time=int(message.text))

    await bot.send_message(chat_id=message.chat.id, text="Хорошо, а времени для маленьких перерывов сколько зададим?")


# Установка таймера малого отдыха
@dp.message_handler(lambda message: not message.text.isdigit(), state=TomatoTimer.s_rest_time)
async def process_s_rest_time_invalid(message: types.Message):
    return await message.reply("Время маленького отдыха нужно бы указать числом в минутах\n"
                               "Так что, сколько минут отдыхаем в маленькие перерывы?")


@dp.message_handler(lambda message: message.text.isdigit(), state=TomatoTimer.s_rest_time)
async def process_s_rest_time(message: types.Message, state: FSMContext):
    await TomatoTimer.next()
    await state.update_data(s_rest_time=int(message.text))
    await TomatoTimer.next()
    await state.update_data(total_tomatos=0)

    await bot.send_message(chat_id=message.chat.id,
                            text="Все настроили, теперь напиши /start_tomato и погнали работать!")


@dp.message_handler()
async def echo(message: types.Message, state: FSMContext):
    if message.text == "Да, включи таймер на отдых":
        async with state.proxy() as data:
            await bot.send_message(chat_id=message.chat.id,
                                   text=f"⌛ Таймер для отдыха на {data['s_rest_time']} минут поставлен!")
            await asyncio.sleep(data['s_rest_time'] * 60)
            await state.update_data(total_tomatos=int(data['total_tomatos'] + 1))

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("Запускай новый рабочий цикл")
            markup.add("Нет, на сегодня хватит!")
            markup.one_time_keyboard = True

            await bot.send_message(chat_id=message.chat.id,
                                   text="Отдых окончен, солнце еще не село 😈, пошли работать?",
                                   reply_markup=markup)

    elif message.text == "Нет, погнали следующий рабочий цикл":
        async with state.proxy() as data:
            await bot.send_message(chat_id=message.chat.id,
                                   text=f"Сколько энтузиазма 😱\nЗапускаю следующий рабочий период!")
            await state.update_data(total_tomatos=int(data['total_tomatos'] + 1))

            await bot.send_message(chat_id=message.chat.id,
                                   text=f"⌛ Таймер для работы на {data['work_time']} минут поставлен!")
            await asyncio.sleep(data['work_time'] * 60)  # TODO: добавить * 60

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("Да, включи таймер на отдых")
            markup.add("Нет, погнали следующий рабочий цикл")
            markup.one_time_keyboard = True

            await bot.send_message(chat_id=message.chat.id, text="🥳 Пора отдохнуть, не так ли?", reply_markup=markup)

    elif message.text == "Запускай новый рабочий цикл":
        async with state.proxy() as data:
            await bot.send_message(chat_id=message.chat.id,
                                   text=f"⌛ Таймер для работы на {data['work_time']} минут поставлен!")
            await asyncio.sleep(data['work_time'] * 60)  # TODO: добавить * 60

            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("Да, включи таймер на отдых")
            markup.add("Нет, погнали следующий рабочий цикл")
            markup.one_time_keyboard = True

            await bot.send_message(chat_id=message.chat.id, text="🥳 Пора отдохнуть, не так ли?", reply_markup=markup)

    elif message.text == "Нет, на сегодня хватит!":
        async with state.proxy() as data:
            await bot.send_message(chat_id=message.chat.id,
                                   text=f"Хорошо поработал, всего удалось выполнить {data['total_tomatos']} помидорки!"
                                        f"\nЧтобы начать завтра вновь, напиши /start_tomato")

    else:
        await bot.send_message(chat_id=message.chat.id,
                               text="Не, я только помидорки могу настроить и запустить, а не понять тебя 🤪")

# Запускаем пулл
if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
